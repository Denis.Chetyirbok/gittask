trigger ProductTableTrigger on ProductTable__c (before insert, before update) {
    //Available__c && AddedDate__c
    if(Trigger.isBefore && Trigger.isInsert){
        ProductTableTriggerHelper.avaivableCheck(Trigger.new);
        ProductTableTriggerHelper.addedDateCheck(Trigger.new);
    }        
    //Available__c
    if(Trigger.isBefore && Trigger.isUpdate){
        ProductTableTriggerHelper.avaivableCheck(Trigger.new);
    }   
}